from telegram.ext import Updater, CommandHandler
import sys
import json
import os
import logging
import redis
import pickle
from urllib.request import urlopen


class TBot(object):
    """docstring for TBot"""

    def __init__(self, config):
        super(TBot, self).__init__()
        self.config = config
        self.storage = redis.StrictRedis(self.config['REDIS_SERVER'])
        self.updater = Updater(self.config['TOKEN'])

        self.updater.dispatcher.add_handler(CommandHandler('start', self.start))
        self.updater.dispatcher.add_handler(CommandHandler('today', self.today))

        self.updater.start_polling()

    def start(self, bot, update):
        bot.sendMessage(chat_id=update.message.chat_id, text=config['HELLO_MESSAGE'])

    def today(self, bot, update):
        data = self.storage.get(self.config['TASK_NAME'])
        if not data:
            logging.info('Дергаем ручку')
            urlopen(config['FILL_CACHE'])
            data = self.storage.get(self.config['TASK_NAME'])
        data = pickle.loads(data)['today']
        bot.sendMessage(chat_id=update.message.chat_id, text='Сьогодні відбувається %i події(й)' % len(data))
        for event in data:
            # logging.info(event['SUMMARY'])
            text = 'Що: {summary}\nДе: {location}\nКоли: {time}\n\n{url}'.format(summary=event['SUMMARY'], location=event['LOCATION'], time=event['DTSTART'], url=event['URL'])
            bot.sendMessage(chat_id=update.message.chat_id, text=text)



CONF_DIR = './conf'


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

# Название задачи
task = sys.argv[1]
# Загружаем конфиг
config = json.load(open(os.path.join(CONF_DIR, task + '.conf'), 'r', encoding='utf-8'))

bot = TBot(config)
