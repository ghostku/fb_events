import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from fb_events.telegram import TelegramBot
from flask_redis import FlaskRedis
import logging
import logging.config

app = Flask(__name__)
app.config.from_object(os.environ['FB_EVENTS'])
db = SQLAlchemy(app)
tbot = TelegramBot(app.config['TELEGRAM_BOT_TOKEN'])
storage = FlaskRedis(app)

logging.config.dictConfig(app.config['LOG'])
log = logging.getLogger('fb_events')
cronlog = logging.getLogger('fb_events.cron')

from fb_events import views, models
