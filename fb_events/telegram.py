from telegram.bot import Bot
import logging

class TelegramBot(Bot):
    """docstring for TelegramBot"""

    def __init__(self, token):
        super(TelegramBot, self).__init__(token)

    def post_events(self, events, chat_id):
        for event in events:
            text = 'Що: {summary}\nДе: {location}\nКоли: {time}\n\n{url}'.format(summary=event.summary, location=event.location, time=event.dtstart, url=event.url)
            # logging.debug(text)
            self.send_message(chat_id, text)
