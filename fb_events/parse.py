from fb_events import app, db, models, log, cronlog
from fb_events.models import Event
import urllib.request
from icalendar import Calendar


class Wallflux(object):
    """docstring for Wallflux"""

    GROUP_EVENTS_URL = 'http://wallflux.com/events/{group_id}'

    def __init__(self):
        super(Wallflux, self).__init__()

    def get_group_events(self, group_id):
        data = urllib.request.urlopen(self.GROUP_EVENTS_URL.format(group_id=group_id)).read().decode('utf-8')
        data = data.replace('\\"', '"')
        data = data.replace('\\n\\nEvent displayed using Wallflux.com', '')
        return data


def get_events(config):
    cronlog.info('%s - Parse Start' % config['TASK_NAME'])
    wf = Wallflux()
    uids = [i.uid for i in db.session.query(Event).filter(Event.task == config['TASK_NAME']).all()]
    new_events = []
    try:
        for group in config['GROUPS']:
            for vevent in Calendar().from_ical(wf.get_group_events(group['ID'])).subcomponents:
                if vevent['UID'] not in uids:
                    event = Event(task=config['TASK_NAME']).from_ical(vevent)
                    new_events.append(event)
                    db.session.add(event)
    except KeyError:
        cronlog.error('%s - Parse Error' % config['TASK_NAME'])
        return []  # Если бы ошибка возвращаем пустой список
    else:  # Если не было ошибки - делаем запись в базу
        db.session.commit()
        cronlog.info('%s - Parse End' % config['TASK_NAME'])
    return new_events
