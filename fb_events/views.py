from flask import render_template, request, redirect, flash, Response, url_for, send_file
from fb_events import app, parse, tbot, storage, log
from fb_events.models import Event as dbEvent
import os
import json
import pickle
from werkzeug.contrib.atom import AtomFeed
from icalendar import Calendar
from datetime import datetime, timedelta


def get_conf(task):
    return json.load(open(os.path.join(app.config['CONF_DIR'], task + '.conf'), 'r', encoding='utf-8'))


# @app.route('/telegram_post_all/<task>')
# def test(task):
#     config = get_conf(task)
#     new_events = Event.query.limit(25).all()
#     tbot.post_events(new_events, config['TELEGRAM_CHAT'])
#     return '%i new events added' % len(new_events)


@app.route('/test')
def test():
    log.error('Achtung')
    return 'ERROR'


@app.route('/<task>/clean_cache')
def clean_cache(task):
    storage.delete(task)
    return 'Done'


@app.route('/<task>/fill_cache')
def fill_cache(task):
    config = get_conf(task)
    query = dbEvent.query.filter(dbEvent.task == config['TASK_NAME'])
    today = datetime.today().date()
    query = query.filter(dbEvent.dtstart < today + timedelta(days=1)).filter(dbEvent.dtend >= today)
    today_events = [i.jsonify() for i in query.all()]
    storage.set(task, pickle.dumps({'today': today_events}))
    return str(len(today_events))


@app.route('/<task>/cal')
def show_calendar(task):
    config = get_conf(task)
    return redirect(config['GOOGLE_CALENDAR_URL'])


@app.route('/parse/<task>')
@app.route('/<task>/parse')
def parse_events(task):
    task = task.replace('.', '_')
    config = get_conf(task)
    new_events = parse.get_events(config)
    tbot.post_events(new_events, config['TELEGRAM_CHAT'])
    return '%i new events added' % len(new_events)


@app.route('/atom/<task>')
@app.route('/<task>/atom')
def get_atom(task):
    task = task.replace('.', '_')
    config = get_conf(task)
    feed = AtomFeed(config['PROJECT_NAME'], feed_url=request.url, url=config['URL'])
    events = dbEvent.query.filter(dbEvent.task == config['TASK_NAME']).order_by(dbEvent.dtstamp.desc()).limit(25).all()
    for event in events:
        feed.add(event.organiser + ': ' + event.summary, event.description, content_type='html', url=event.url, updated=event.dtstamp)
    return feed.get_response()


@app.route('/ical/<task>')
@app.route('/<task>/ical')
def get_ical(task):
    task = task.replace('.', '_')
    config = get_conf(task)
    events = dbEvent.query.filter(dbEvent.task == config['TASK_NAME']).order_by(dbEvent.dtstamp.desc()).limit(100).all()
    cal = Calendar()
    cal.add('prodid', '-//wallflux.com/calendar//NONSGML v1.0//EN')
    cal.add('version', '2.0')
    cal.add('X-WR-CALNAME', config['PROJECT_NAME'])
    cal.add('X-PUBLISHED-TTL', 'PT1H')
    cal.add('X-ORIGINAL-URL', request.url)
    cal.add('X-WR-TIMEZONE', config['DEFAULT_TIME_ZONE'])
    cal.add('CALSCALE', 'GREGORIAN')
    cal.add('METHOD', 'PUBLISH')
    for db_event in events:
        cal.add_component(db_event.to_ical(config['DEFAULT_TIME_ZONE']))
    data = cal.to_ical().decode('utf-8').replace('\r\n ', '')
    return Response(data, mimetype='text/calendar', headers={'Content-Disposition': 'attachment;filename=' + config['TASK_NAME'] + '.ics'})
