from fb_events import db, log
from icalendar import Event as CalEvent, vCalAddress, vText
from pytz import timezone

class Event(db.Model):
    __tablename__ = 'events'
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(50))
    task = db.Column(db.String(25))
    attach = db.Column(db.String(50))
    description = db.Column(db.String(300))
    url = db.Column(db.String(50))
    location = db.Column(db.String(100))
    organiser = db.Column(db.String(50))
    summary = db.Column(db.String(50))
    dtend = db.Column(db.DateTime)
    dtstart = db.Column(db.DateTime)
    dtstamp = db.Column(db.DateTime)

    def from_ical(self, cal):
        try:
            self.uid = cal['UID']
            self.attach = cal['ATTACH']
            self.description = cal.get('DESCRIPTION', '')
            self.url = cal['URL']
            self.location = cal.get('LOCATION', '')
            self.organiser = cal['ORGANIZER'].params['CN']
            self.summary = cal['SUMMARY']
            self.dtend = cal.decoded('DTEND')
            self.dtstart = cal.decoded('DTSTART')
            self.dtstamp = cal.decoded('DTSTAMP')
        except KeyError:
            log.error(cal)
            raise
        return self

    def to_ical(self, default_tz):
        event = CalEvent()
        event.add('UID', self.uid)
        event.add('ATTACH', self.attach)
        # event.add('DESCRIPTION', self.description)
        # log.debug(self.description)
        event['description'] = vText(self.url + '\n\n' + self.description)
        # log.debug(event['description'])
        event.add('URL', self.url)
        event.add('LOCATION', self.location)
        event.add('ORGANIZER', self.organiser)
        event.add('SUMMARY', self.summary)
        event.add('DTEND', timezone(default_tz).localize(self.dtend))
        event.add('DTSTART', timezone(default_tz).localize(self.dtstart))

        # event['DTSTART'].params['TZID'] = default_tz
        # event['DTEND'].params['TZID'] = default_tz
        event.add('DTSTAMP', self.dtstamp)
        return event

    def jsonify(self):
        return {
            'UID': self.uid,
            'ATTACH': self.attach,
            'DESCRIPTION': self.description,
            'URL': self.url,
            'LOCATION': self.location,
            'ORGANIZER': self.organiser,
            'SUMMARY': self.summary,
            'DTEND': self.dtend,
            'DTSTART': self.dtstart,
            'DTSTAMP': self.dtstamp
        }