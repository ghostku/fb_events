README

Installation
____________

1. sudo apt-get update && sudo apt-get upgrade
2. sudo apt-get install apache2 python3-dev libapache2-mod-wsgi-py3 python3-pip
3. sudo a2enmod wsgi
4. sudo pip3 install --upgrade pip
5. sudo pip3 install Flask sqlalchemy flask-sqlalchemy sqlalchemy-migrate flask_migrate icalendar python-telegram-bot flask-redis
6. cd /var/www
7. git clone ssh://git@bitbucket.org/ghostku/fb_events.git
8. cd fb_events
9. mkdir  data
10. ln -s apache-site-config/fb_events.conf /etc/apache2/sites-available/fb_events.conf
11. sudo a2ensite fb_events
12. sudo service apache2 restart
13. ./manage.py db init && ./manage.py db migrate && ./manage.py db upgrade
14 Go to dns.he.net and create fb_events.hermes.biz.ua


# TODO
TODO: Поддержка TimeZone
TODO: Свой парсер для ивента
TODO: Бот в телеграме
TODO: Проверить берет ли от группы
TODO: Ручное добавление ивентов
TODO: Поддержка твиттера
TODO: Переезд на Heroku
TODO: Переписать TelegramBot по людски