#!/usr/bin/python3
from fb_events import app
import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
app.run(debug=True, host='0.0.0.0')
