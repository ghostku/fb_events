#!/usr/bin/python3
import sys
import os
import logging

logging.basicConfig(stream=sys.stderr, format='%(asctime)s - %(levelname)s - %(message)s')
sys.path.insert(0, "/var/www/fb_events/")
os.environ['FB_EVENTS']='config.ProductionConfig'
from fb_events import app as application
